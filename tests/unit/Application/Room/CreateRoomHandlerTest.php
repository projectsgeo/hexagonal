<?php
declare(strict_types=1);

namespace Tests\Unit\Application\Room;

use Codeception\Test\Unit;
use Hexagonal\Application\Room\CreateRoomHandler;
use Hexagonal\Application\Room\Dto\RoomResponse;
use Hexagonal\Domain\Hotel\Service\HotelFinder;
use Hexagonal\Domain\Room\Model\Room;
use Hexagonal\Domain\Room\Repository\RoomRepositoryInterface;
use Hexagonal\Domain\Shared\Exception\InvalidSharedException;
use Psr\Log\LoggerInterface;
use Tests\Data\Application\Room\Dto\CreateRoomRequestFaker;

class CreateRoomHandlerTest extends Unit
{
    private CreateRoomHandler $handler;
    private HotelFinder $hotelFinder;

    protected function _before()
    {
        $this->hotelFinder = $this->createMock(HotelFinder::class);
        $this->handler = new CreateRoomHandler(
            $this->createMock(RoomRepositoryInterface::class),
            $this->hotelFinder,
            $this->createMock(LoggerInterface::class)
        );
    }

    public function test_can_create_a_room_use_case()
    {
        $response = ($this->handler)(CreateRoomRequestFaker::create());
        $this->assertInstanceOf(RoomResponse::class, $response);
        $this->assertInstanceOf(Room::class, $response->room());
        $this->assertTrue($response->isStatusOk());
        $this->assertMatchesRegularExpression('~Room created to hotel .* successfully with following id: .*~', $response->message());
    }

    public function test_cannot_creat_a_room_use_case_because_hotel_not_exists()
    {
        $this->hotelFinder->method('__invoke')->will($this->throwException(InvalidSharedException::entityNotFound()));
        $response = ($this->handler)(CreateRoomRequestFaker::create());
        $this->assertInstanceOf(RoomResponse::class, $response);
        $this->assertNull($response->room());
        $this->assertFalse($response->isStatusOk());
        $this->assertMatchesRegularExpression('~Something went wrong. Not found~', $response->message());
    }
}