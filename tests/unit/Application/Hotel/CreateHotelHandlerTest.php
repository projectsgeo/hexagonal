<?php
declare(strict_types=1);

namespace Tests\Unit\Application\Hotel;

use Codeception\Test\Unit;
use Exception;
use Hexagonal\Application\Hotel\CreateHotelHandler;
use Hexagonal\Application\Hotel\Dto\HotelResponse;
use Hexagonal\Domain\Hotel\Model\Hotel;
use Hexagonal\Domain\Hotel\Repository\HotelRepositoryInterface;
use Psr\Log\LoggerInterface;
use Tests\Data\Application\Hotel\Dto\CreateHotelRequestFaker;

class CreateHotelHandlerTest extends Unit
{
    private CreateHotelHandler $handler;
    private HotelRepositoryInterface $hotelRepository;

    protected function _before()
    {
        $this->hotelRepository = $this->createMock(HotelRepositoryInterface::class);
        $this->handler = new CreateHotelHandler($this->hotelRepository, $this->createMock(LoggerInterface::class));
    }

    public function test_can_create_a_hotel_use_case()
    {
        $response = ($this->handler)(CreateHotelRequestFaker::create());
        $this->assertInstanceOf(HotelResponse::class, $response);
        $this->assertInstanceOf(Hotel::class, $response->hotel());
        $this->assertTrue($response->isStatusOk());
        $this->assertStringContainsString('Hotel created successfully with the following id: ', $response->message());
    }

    public function test_cannot_create_a_hotel_use_case_because_repository_throw_an_exception()
    {
        $errorMessage = 'Persistence timeout';
        $this->hotelRepository->method('store')->will($this->throwException(new Exception($errorMessage)));
        $response = ($this->handler)(CreateHotelRequestFaker::create());
        $this->assertInstanceOf(HotelResponse::class, $response);
        $this->assertNull($response->hotel());
        $this->assertFalse($response->isStatusOk());
        $this->assertStringContainsString($errorMessage, $response->message());
    }
}