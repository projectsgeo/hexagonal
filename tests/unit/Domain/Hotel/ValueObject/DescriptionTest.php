<?php
declare(strict_types=1);

namespace Tests\Unit\Domain\Hotel\ValueObject;

use Codeception\Test\Unit;
use Hexagonal\Domain\Hotel\Exception\InvalidHotelException;
use Hexagonal\Domain\Hotel\ValueObject\Description;

class DescriptionTest extends Unit
{
    public function test_can_create_from_content()
    {
        $content = 'Test description';
        $instance = Description::create($content);
        $this->assertInstanceOf(Description::class, $instance);
        $this->assertStringContainsString($content, $instance->value());
    }

    public function test_too_short_content_throws_exception()
    {
        $content = str_repeat("6", intval(Description::MIN_LENGTH) - 1);
        $this->expectException(InvalidHotelException::class);
        $this->expectErrorMessage(
            'Description value is invalid. Length must be between ' . Description::MIN_LENGTH .' and ' . Description::MAX_LENGTH);
        Description::create($content);
    }

    public function test_too_long_content_throws_exception()
    {
        $content = str_repeat("6", intval(Description::MAX_LENGTH) + 1);
        $this->expectException(InvalidHotelException::class);
        $this->expectErrorMessage(
            'Description value is invalid. Length must be between ' . Description::MIN_LENGTH .' and ' . Description::MAX_LENGTH);
        Description::create($content);
    }

    public function test_is_equal_description()
    {
        $descOne = Description::create('description one');
        $descTwo = Description::create('description one');
        $this->assertTrue($descOne->equals($descTwo));
    }

    public function test_is_not_equal_description()
    {
        $descOne = Description::create('description one');
        $descTwo = Description::create('description two');
        $this->assertFalse($descOne->equals($descTwo));
    }
}