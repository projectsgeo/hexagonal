<?php
declare(strict_types=1);

namespace Tests\Unit\Domain\Shared\ValueObject;

use Codeception\Test\Unit;
use Hexagonal\Domain\Shared\Exception\InvalidSharedException;
use Hexagonal\Domain\Shared\ValueObject\Country;

class CountryTest extends Unit
{
    public function test_can_create_country_from_iso_code()
    {
        $country = Country::fromIsoCode('LK');
        $this->assertInstanceOf(Country::class, $country);
        $this->assertEquals('Sri Lanka', $country->name());
    }

    public function test_can_create_country_from_name()
    {
        $country = Country::fromName('Belarus');
        $this->assertInstanceOf(Country::class, $country);
        $this->assertEquals('BY', $country->isoCode());
    }

    public function test_can_check_two_countries_are_equals()
    {
        $country1 = Country::fromIsoCode('EC');
        $country2 = Country::fromName('Ecuador');
        $this->assertTrue($country1->equals($country2));
    }

    public function test_can_not_create_a_country_from_not_existent_iso_code()
    {
        $this->expectException(InvalidSharedException::class);
        $this->expectErrorMessage('IsoCode provided: XX is not a valid ISO 3166-1 alpha-2.');
        Country::fromIsoCode('XX');
    }

    public function test_can_not_create_a_country_from_not_existent_country_name()
    {
        $this->expectException(InvalidSharedException::class);
        $this->expectErrorMessage('Name provided: ZOU is not a valid country name.');
        Country::fromName('ZOU');
    }

    /**
     * @dataProvider isoCodeWithInvalidLengthProvider
     */
    public function test_iso_code_with_invalid_length_throws_exception(string $isoCode)
    {
        $this->expectException(InvalidSharedException::class);
        $this->expectErrorMessage('Country ISO Code must have 2 characters. Length given: ' . strlen($isoCode));
        Country::fromIsoCode($isoCode);
    }

    public function isoCodeWithInvalidLengthProvider(): array
    {
        return [
            ['DEUS'],
            ['D'],
            [''],
        ];
    }
}