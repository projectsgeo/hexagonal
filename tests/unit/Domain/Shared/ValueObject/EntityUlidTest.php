<?php
declare(strict_types=1);

namespace Tests\Unit\Domain\Shared\ValueObject;

use Codeception\Test\Unit;
use Hexagonal\Domain\Shared\ValueObject\EntityUlid;
use InvalidArgumentException;

class EntityUlidDummy extends EntityUlid {}

class EntityUlidTest extends Unit
{
    public function test_can_instance_from_null()
    {
        $entityUlidDummy = new EntityUlidDummy();
        $this->assertInstanceOf(EntityUlidDummy::class, $entityUlidDummy);
    }

    public function test_can_instance_from_valid_ulid()
    {
        $entityUlidDummy = new EntityUlidDummy('01EK205KDPRR3KNR0KZR2PNZ6R');
        $this->assertInstanceOf(EntityUlidDummy::class, $entityUlidDummy);
    }

    public function test_cannot_instance_from_invalid_ulid()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectErrorMessageMatches('~Invalid ULID.*~');
        new EntityUlidDummy('01EK205KDPRR3KNR0KZR2PNZ6R666');
    }

    public function test_same_ulid_comparison_equals_true()
    {
        $entityUlidDummy1 = new EntityUlidDummy('01EK205KDPRR3KNR0KZR2PNZ6R');
        $entityUlidDummy2 = new EntityUlidDummy('01EK205KDPRR3KNR0KZR2PNZ6R');
        $this->assertTrue($entityUlidDummy1->equals($entityUlidDummy2));
    }

    public function test_different_ulid_comparison_equals_false()
    {
        $entityUlidDummy1 = new EntityUlidDummy();
        $entityUlidDummy2 = new EntityUlidDummy();
        $this->assertFalse($entityUlidDummy1->equals($entityUlidDummy2));
    }
}