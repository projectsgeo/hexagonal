<?php
declare(strict_types=1);

namespace Tests\Unit\Domain\Shared\ValueObject;

use Codeception\Test\Unit;
use Hexagonal\Domain\Shared\ValueObject\EntityUuid;
use InvalidArgumentException;

class EntityUuidDummy extends EntityUuid {}

class EntityUuidTest extends Unit
{
    public function test_can_instance_from_null()
    {
        $entityUuidDummy = new EntityUuidDummy();
        $this->assertInstanceOf(EntityUuidDummy::class, $entityUuidDummy);
        $this->assertRegExp('/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/', $entityUuidDummy->value());
    }

    public function test_can_instance_from_valid_uuid_v4()
    {
        $entityUuidDummy = new EntityUuidDummy('ac410cbc-e9f0-4b2d-ae7e-e320e8bc2072');
        $this->assertInstanceOf(EntityUuidDummy::class, $entityUuidDummy);
        $this->assertEquals('ac410cbc-e9f0-4b2d-ae7e-e320e8bc2072', $entityUuidDummy->value());
    }

    public function test_cannot_instance_from_invalid_uuid()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectErrorMessageMatches('~Invalid UUID.*~');
        new EntityUuidDummy('ac410cbc-e9f0-4b2d-ae7e');
    }

    public function test_same_uuid_comparison_equals_true()
    {
        $entityUuidDummy1 = new EntityUuidDummy('ac410cbc-e9f0-4b2d-ae7e-e320e8bc2072');
        $entityUuidDummy2 = new EntityUuidDummy('ac410cbc-e9f0-4b2d-ae7e-e320e8bc2072');
        $this->assertTrue($entityUuidDummy1->equals($entityUuidDummy2));
    }

    public function test_different_uuid_comparison_equals_false()
    {
        $entityUuidDummy1 = new EntityUuidDummy();
        $entityUuidDummy2 = new EntityUuidDummy();
        $this->assertFalse($entityUuidDummy1->equals($entityUuidDummy2));
    }
}