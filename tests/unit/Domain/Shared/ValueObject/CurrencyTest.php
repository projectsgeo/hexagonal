<?php
declare(strict_types=1);

namespace Tests\Unit\Domain\Shared\ValueObject;

use Codeception\Test\Unit;
use Hexagonal\Domain\Shared\Exception\InvalidSharedException;
use Hexagonal\Domain\Shared\ValueObject\Currency;

class CurrencyTest extends Unit
{
    public function test_can_create_from_iso_code()
    {
        $currency = Currency::fromIsoCode('eur');
        $this->assertInstanceOf(Currency::class, $currency);
        $this->assertEquals('EUR', $currency->isoCode());
    }

    public function test_throw_exception_if_iso_code_is_not_considered()
    {
        $this->expectException(InvalidSharedException::class);
        $this->expectErrorMessage('Given iso code: CSK, not supported or invalid');
        Currency::fromIsoCode('CSK');
    }
}