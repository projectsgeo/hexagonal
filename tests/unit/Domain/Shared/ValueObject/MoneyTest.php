<?php
declare(strict_types=1);

namespace Tests\Unit\Domain\Shared\ValueObject;

use Codeception\Test\Unit;
use Hexagonal\Domain\Shared\Exception\InvalidSharedException;
use Hexagonal\Domain\Shared\ValueObject\Currency;
use Hexagonal\Domain\Shared\ValueObject\Money;
use Tests\Data\Domain\Shared\ValueObject\CurrencyFaker;

class MoneyTest extends Unit
{
    private const EUR_CURRENCY = 'EUR';

    public function test_can_create()
    {
        $money = Money::create( 100, CurrencyFaker::create());
        $this->assertInstanceOf(Money::class, $money);
        $this->assertEquals(100, $money->amount());
    }

    public function test_throw_exception_if_amount_is_negative()
    {
        $this->expectException(InvalidSharedException::class);
        $this->expectErrorMessage('Amount must be positive. Value given: -1');
        Money::create( -1, CurrencyFaker::create());
    }

    public function test_same_money_amount_equals_true()
    {
        $money1 = Money::create(10, Currency::fromIsoCode('EUR'));
        $money2 = Money::create(10, Currency::fromIsoCode('EUR'));
        $this->assertTrue($money1->equals($money2));
    }

    public function test_different_money_amount_equals_false()
    {
        $money1 = Money::create(10, Currency::fromIsoCode('EUR'));
        $money2 = Money::create(666, Currency::fromIsoCode('USD'));
        $this->assertFalse($money1->equals($money2));
    }

    public function test_different_money_currency_equals_false()
    {
        $money1 = Money::create(10, Currency::fromIsoCode('EUR'));
        $money2 = Money::create(10, Currency::fromIsoCode('USD'));
        $this->assertFalse($money1->equals($money2));
    }

    public function test_can_sub_quantity()
    {
        $money1 = Money::create(10, Currency::fromIsoCode(self::EUR_CURRENCY));
        $this->assertEquals(8, $money1->sub(Money::create(2, Currency::fromIsoCode(self::EUR_CURRENCY),))->amount());
    }

    public function test_can_not_sub_quantity_different_currency()
    {
        $money1 = Money::create(10, Currency::fromIsoCode(self::EUR_CURRENCY));
        $this->expectException(InvalidSharedException::class);
        $this->expectExceptionMessage('Currencies must match to process operations. Given currencies: USD : EUR');
        $money1->sub(Money::create(2, Currency::fromIsoCode('USD')))->amount();
    }

    public function test_can_add_quantity()
    {
        $money1 = Money::create(333, Currency::fromIsoCode(self::EUR_CURRENCY));
        $this->assertEquals(666, $money1->add(Money::create(333, Currency::fromIsoCode(self::EUR_CURRENCY)))->amount());
    }

    public function test_can_not_add_quantity_different_currency()
    {
        $money1 = Money::create(1, Currency::fromIsoCode(self::EUR_CURRENCY));
        $this->expectException(InvalidSharedException::class);
        $this->expectExceptionMessage('Currencies must match to process operations. Given currencies: USD : EUR');
        $money1->add(Money::create(2, Currency::fromIsoCode('USD')))->amount();
    }

    public function test_can_multiply()
    {
        $money1 = Money::create(10, CurrencyFaker::create());
        $this->assertEquals(30, $money1->multiply(3.0)->amount());
    }

    public function testCanRender()
    {
        $money = Money::create(100099, Currency::fromIsoCode(self::EUR_CURRENCY));
        $this->assertEquals('1.000,99€', $money->render());
    }
}