<?php
declare(strict_types=1);

namespace Tests\Unit\Infrastructure\Repository\MySql\Model;

use Codeception\Test\Unit;
use Hexagonal\Domain\Hotel\Repository\HotelRepositoryInterface;
use Tests\Data\Domain\Hotel\Model\HotelFaker;

class HotelRepositoryTest extends Unit
{
    private HotelRepositoryInterface $hotelRepository;

    protected function _before()
    {
        $this->hotelRepository = $this->getModule('Symfony')->grabService(HotelRepositoryInterface::class);
    }

    public function test_can_save_a_hotel()
    {
        $hotel = HotelFaker::create();
        $this->hotelRepository->store($hotel);
    }

    public function test_can_find_an_existing_video()
    {
        $hotel = HotelFaker::create();
        $this->hotelRepository->store($hotel);
        $this->clearEntityManager();
        $this->assertEquals($hotel, $this->hotelRepository->byHotelId($hotel->id()));
    }

    private function clearEntityManager(): void
    {
        $this->getModule('Doctrine2')->clearEntityManager();
    }
}