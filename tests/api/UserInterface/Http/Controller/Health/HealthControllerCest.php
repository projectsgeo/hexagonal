<?php
declare(strict_types=1);

namespace Tests\Api\UserInterface\Http\Controller\Health;

use Codeception\Util\HttpCode;
use Tests\ApiTester;

class HealthControllerCest
{
    const HEALTH_URL = '/health';
    const BASE_URL = '/';

    public function testHealthIsResponding(ApiTester $I)
    {
        $I->wantTo('Health endpoint is responding 200');
        $I->sendGET(static::HEALTH_URL);
        $I->seeResponseCodeIs(HttpCode::OK);
    }

    public function testHealthIsRespondingWithRootPath(ApiTester $I)
    {
        $I->wantTo('Home endpoint is redirected to health endpoint and responding 200');
        $I->sendGET(static::BASE_URL);
        $I->seeResponseCodeIs(HttpCode::OK);
    }

    public function testHealthResponseIsJson(ApiTester $I)
    {
        $I->wantTo('Health endpoint is responding a json');
        $I->sendGET(static::HEALTH_URL);
        $I->seeResponseIsJson();
    }

    public function testHealthResponseWithCorrectStructure(ApiTester $I)
    {
        $I->wantTo('Health endpoint is responding with correct structure');
        $I->sendGET(static::HEALTH_URL);
        $I->seeResponseMatchesJsonType([
            'status' => 'string',
            'version' => 'string',
            'docker_node' => 'string'
        ]);
    }
}