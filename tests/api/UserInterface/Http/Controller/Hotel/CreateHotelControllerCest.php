<?php
declare(strict_types=1);

namespace Tests\Api\UserInterface\Http\Controller\Hotel;

use Codeception\Util\HttpCode;
use Hexagonal\Application\Shared\Dto\HexagonalResponse;
use Hexagonal\Domain\Hotel\Model\Hotel;
use Tests\ApiTester;
use Tests\Data\BaseFaker;
use Tests\Data\Domain\Hotel\ValueObject\DescriptionFaker;
use Tests\Data\Domain\Shared\ValueObject\CityFaker;
use Tests\Data\Domain\Shared\ValueObject\CountryFaker;
use Tests\Data\Domain\Shared\ValueObject\LocationFaker;
use Tests\Data\Domain\Shared\ValueObject\StateFaker;

class CreateHotelControllerCest
{
    private const HOTEL_URL = '/hotel';

    public function _before(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
    }

    public function test_can_create_hotel(ApiTester $I)
    {
        $request = $this->request();
        $I->sendPOST(self::HOTEL_URL, $request);
        $I->seeResponseCodeIs(HttpCode::CREATED);
        $I->seeResponseContainsJson(['status' => HexagonalResponse::STATUS_OK]);

        $I->seeResponseMatchesJsonType(['hotel' => [
            'id' => 'string',
            'address' => 'array',
        ]]);

        /*$I->seeInRepository(Hotel::class, [
            'description.value' => $request['description'],
            'address.country.isoCode' => $request['country'],
        ]);*/
    }

    public function test_can_not_create_hotel_because_country_not_exists(ApiTester $I)
    {
        $request = $this->request('XX');
        $I->sendPOST(self::HOTEL_URL, $request);
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);
        $I->seeResponseContainsJson(['status' => HexagonalResponse::STATUS_KO]);
        $I->seeResponseMatchesJsonType(['message' => 'string:regex(~IsoCode provided: XX is not a valid ISO 3166-1 alpha-2~)']);
    }

    private function request(string $country = null): array
    {
        return [
            'description' => DescriptionFaker::create()->value(),
            'country' => $country ?? CountryFaker::create()->isoCode(),
            'location' => LocationFaker::create()->value(),
            'state'  => StateFaker::create()->value(),
            'city' => CityFaker::create()->value(),
            'postal_code' => BaseFaker::faker()->postcode,
        ];
    }
}