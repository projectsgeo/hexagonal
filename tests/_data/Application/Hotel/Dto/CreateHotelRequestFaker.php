<?php
declare(strict_types=1);

namespace Tests\Data\Application\Hotel\Dto;

use Hexagonal\Application\Hotel\Dto\CreateHotelRequest;
use Tests\Data\Domain\Hotel\ValueObject\AddressFaker;
use Tests\Data\Domain\Hotel\ValueObject\DescriptionFaker;

class CreateHotelRequestFaker
{
    public static function create(): CreateHotelRequest
    {
        return new CreateHotelRequest(
            AddressFaker::create(),
            DescriptionFaker::create()
        );
    }
}