<?php
declare(strict_types=1);

namespace Tests\Data\Application\Room\Dto;

use Hexagonal\Application\Room\Dto\CreateRoomRequest;
use Hexagonal\Domain\Hotel\ValueObject\HotelId;
use Tests\Data\Domain\Room\ValueObject\FloorFaker;
use Tests\Data\Domain\Room\ValueObject\RoomNumberFaker;
use Tests\Data\Domain\Shared\ValueObject\MoneyFaker;

class CreateRoomRequestFaker
{
    public static function create(): CreateRoomRequest
    {
        return new CreateRoomRequest(
            new HotelId(),
            FloorFaker::create(),
            RoomNumberFaker::create(),
            MoneyFaker::create()
        );
    }
}