<?php
declare(strict_types=1);

namespace Tests\Data\Domain\Room\ValueObject;

use Hexagonal\Domain\Room\ValueObject\RoomNumber;
use Tests\Data\BaseFaker;

class RoomNumberFaker
{
    public static function create(): RoomNumber
    {
        return RoomNumber::create(BaseFaker::faker()->randomNumber(2));
    }
}