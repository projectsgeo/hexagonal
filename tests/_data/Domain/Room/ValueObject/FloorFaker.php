<?php
declare(strict_types=1);

namespace Tests\Data\Domain\Room\ValueObject;

use Hexagonal\Domain\Room\ValueObject\Floor;
use Tests\Data\BaseFaker;

class FloorFaker
{
    public static function create(): Floor
    {
        return Floor::create(BaseFaker::faker()->randomNumber(2));
    }
}