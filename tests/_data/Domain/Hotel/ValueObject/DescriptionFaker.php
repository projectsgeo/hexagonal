<?php
declare(strict_types=1);

namespace Tests\Data\Domain\Hotel\ValueObject;

use Hexagonal\Domain\Hotel\ValueObject\Description;
use Tests\Data\BaseFaker;

class DescriptionFaker
{
    public static function create(): Description
    {
        return Description::create(BaseFaker::faker()->text(Description::MAX_LENGTH));
    }
}