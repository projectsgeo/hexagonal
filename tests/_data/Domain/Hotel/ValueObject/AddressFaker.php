<?php
declare(strict_types=1);

namespace Tests\Data\Domain\Hotel\ValueObject;


use Hexagonal\Domain\Shared\ValueObject\Address;
use Tests\Data\BaseFaker;
use Tests\Data\Domain\Shared\ValueObject\CityFaker;
use Tests\Data\Domain\Shared\ValueObject\CountryFaker;
use Tests\Data\Domain\Shared\ValueObject\LocationFaker;
use Tests\Data\Domain\Shared\ValueObject\StateFaker;

class AddressFaker
{
    public static function create(): Address
    {
        return new Address(
            CountryFaker::create(),
            StateFaker::create(),
            CityFaker::create(),
            LocationFaker::create(),
            BaseFaker::faker()->postcode
        );
    }
}