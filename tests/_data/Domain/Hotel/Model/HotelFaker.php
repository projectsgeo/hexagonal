<?php
declare(strict_types=1);

namespace Tests\Data\Domain\Hotel\Model;

use Hexagonal\Domain\Hotel\Model\Hotel;
use Hexagonal\Domain\Hotel\ValueObject\HotelId;
use Hexagonal\Domain\Shared\ValueObject\Active;
use Tests\Data\Domain\Hotel\ValueObject\AddressFaker;
use Tests\Data\Domain\Hotel\ValueObject\DescriptionFaker;

class HotelFaker
{
    public static function create(): Hotel
    {
        return new Hotel(
            new HotelId(),
            AddressFaker::create(),
            DescriptionFaker::create(),
            Active::create()
        );
    }
}