<?php
declare(strict_types=1);

namespace Tests\Data\Domain\Shared\ValueObject;

use Hexagonal\Domain\Shared\ValueObject\Money;
use Tests\Data\BaseFaker;

class MoneyFaker
{
    public static function create(): Money
    {
        return Money::create(BaseFaker::faker()->randomNumber(), CurrencyFaker::create());
    }
}