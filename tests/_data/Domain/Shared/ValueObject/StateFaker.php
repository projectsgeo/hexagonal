<?php
declare(strict_types=1);

namespace Tests\Data\Domain\Shared\ValueObject;

use Hexagonal\Domain\Shared\ValueObject\State;
use Tests\Data\BaseFaker;

class StateFaker
{
    public static function create(): State
    {
        return State::create(BaseFaker::faker()->state);
    }
}