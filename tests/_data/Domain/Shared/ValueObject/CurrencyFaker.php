<?php
declare(strict_types=1);

namespace Tests\Data\Domain\Shared\ValueObject;

use Hexagonal\Domain\Shared\ValueObject\Currency;
use Tests\Data\BaseFaker;

class CurrencyFaker
{
    public static function create(): Currency
    {
        return Currency::fromIsoCode(BaseFaker::faker()->randomElement(array_keys(Currency::CURRENCIES)));
    }
}