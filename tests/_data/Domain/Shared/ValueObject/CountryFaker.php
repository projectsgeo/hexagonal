<?php
declare(strict_types=1);

namespace Tests\Data\Domain\Shared\ValueObject;

use Hexagonal\Domain\Shared\ValueObject\Country;
use Tests\Data\BaseFaker;

class CountryFaker
{
    public static function create(): Country
    {
        return Country::fromIsoCode(BaseFaker::faker()->countryCode);
    }
}