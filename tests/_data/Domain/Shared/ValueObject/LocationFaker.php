<?php
declare(strict_types=1);

namespace Tests\Data\Domain\Shared\ValueObject;

use Hexagonal\Domain\Shared\ValueObject\Location;
use Tests\Data\BaseFaker;

class LocationFaker
{
    public static function create(): Location
    {
        return Location::create(BaseFaker::faker()->streetAddress);
    }
}