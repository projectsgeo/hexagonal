<?php
declare(strict_types=1);

namespace Tests\Data\Domain\Shared\ValueObject;

use Hexagonal\Domain\Shared\ValueObject\City;
use Tests\Data\BaseFaker;

class CityFaker
{
    public static function create(): City
    {
        return City::create(BaseFaker::faker()->city);
    }
}