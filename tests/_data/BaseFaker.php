<?php
declare(strict_types=1);

namespace Tests\Data;

use Faker\Factory;
use Faker\Generator;

class BaseFaker
{
    private static Generator $faker;

    public static function faker(): Generator
    {
        return self::$faker ?? Factory::create();
    }
}