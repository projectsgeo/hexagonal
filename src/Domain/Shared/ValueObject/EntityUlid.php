<?php
declare(strict_types=1);

namespace Hexagonal\Domain\Shared\ValueObject;

use Stringable;
use Symfony\Component\Uid\Ulid;

abstract class EntityUlid implements Stringable
{
    protected string $value;

    public function __construct(string $value = null)
    {
        $this->value = (new Ulid($value))->__toString();
    }

    public function value(): string
    {
        return $this->value;
    }

    public function equals(EntityUlid $entityUlid): bool
    {
        return $this->value() === $entityUlid->value();
    }

    public function __toString(): string
    {
        return $this->value();
    }
}