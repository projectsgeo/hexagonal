<?php
declare(strict_types=1);

namespace Hexagonal\Domain\Shared\ValueObject;

class Active extends BoolValueObject
{
    public function __construct(bool $value = true)
    {
        parent::__construct($value);
    }

    public static function create(bool $value = true): self
    {
        return parent::create($value);
    }

    public function isActive(): bool
    {
        return $this->value() === true;
    }
}