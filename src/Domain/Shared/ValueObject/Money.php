<?php
declare(strict_types=1);

namespace Hexagonal\Domain\Shared\ValueObject;

use Hexagonal\Domain\Shared\Exception\InvalidSharedException;
use Stringable;

class Money implements Stringable
{
    private const DEFAULT_DECIMALS = 2;
    private const DEFAULT_DECIMAL_POINT = ',';
    private const DEFAULT_THOUSAND_SEPARATOR = '.';
    private const MIN_AMOUNT = 0;

    private int $amount;
    private Currency $currency;

    private function __construct(int $amount, Currency $currency)
    {
        $this->amount = $amount;
        $this->currency = $currency;
    }

    public static function create(int $amount, Currency $currency)
    {
        if ($amount < self::MIN_AMOUNT) {
            throw InvalidSharedException::minimumMoneyValueUnfulfilled(
                'Amount must be positive. Value given: ' . $amount
            );
        }
        return new self($amount, $currency);
    }

    public function amount(): int
    {
        return $this->amount;
    }

    public function currency(): Currency
    {
        return $this->currency;
    }

    public function equals(Money $money): bool
    {
        return $this->currency()->equals($money->currency()) && $this->amount() === $money->amount();
    }

    public function add(Money $qty): Money
    {
        $this->operationConstraints($qty);

        return new self($this->amount() + $qty->amount(), $this->currency());
    }

    public function sub(Money $qty): Money
    {
        $this->operationConstraints($qty);

        return new self($this->amount() - $qty->amount(), $this->currency());
    }

    public function multiply(float $qty): Money
    {
        return new self((int) round($this->amount() * $qty), $this->currency());
    }

    public function render(
        int $decimals = self::DEFAULT_DECIMALS,
        string $decimalPoint = self::DEFAULT_DECIMAL_POINT,
        string $thousandSeparator = self::DEFAULT_THOUSAND_SEPARATOR
    ): string {
        return number_format(
                $this->amount() / 100,
                $decimals,
                $decimalPoint,
                $thousandSeparator
            ) . $this->currency()->symbol();
    }

    public function __toString(): string
    {
        return $this->render();
    }


    private function operationConstraints(Money $money): void
    {
        if (!$this->currency()->equals($money->currency())) {
            throw InvalidSharedException::currenciesDontMatchException(
                sprintf(
                    'Currencies must match to process operations. Given currencies: %s : %s',
                    $money->currency()->isoCode(),
                    $this->currency()->isoCode()
                )
            );
        }
    }
}