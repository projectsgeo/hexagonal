<?php
declare(strict_types=1);

namespace Hexagonal\Domain\Shared\ValueObject;

use Stringable;

abstract class IntValueObject implements Stringable
{
    protected int $value;

    public function __construct(int $value)
    {
        $this->value = $value;
    }

    public static function create(int $value): self
    {
        return new static($value);
    }

    public function value(): int
    {
        return $this->value;
    }

    public function __toString(): string
    {
        return (string) $this->value();
    }
}