<?php
declare(strict_types=1);

namespace Hexagonal\Domain\Shared\ValueObject;

use Hexagonal\Domain\Shared\Exception\InvalidSharedException;
use Hexagonal\Domain\Shared\Interfaces\ArrayRepresentable;
use Stringable;

class Currency implements Stringable, ArrayRepresentable
{
    public const CURRENCIES = [
        'EUR' => '€',
        'USD' => '$',
    ];

    private string $isoCode;
    private string $symbol;

    public function __construct(string $isoCode, string $symbol)
    {
        $this->isoCode = $isoCode;
        $this->symbol = $symbol;
    }

    public function isoCode(): string
    {
        return $this->isoCode;
    }

    public function symbol(): string
    {
        return $this->symbol;
    }
    public static function fromIsoCode(string $isoCode): self
    {
        $isoCode = strtoupper(trim($isoCode));
        self::validIsoCode($isoCode);

        return new self($isoCode, self::CURRENCIES[$isoCode]);
    }

    public function equals(Currency $currency): bool
    {
        return $currency->isoCode() === $this->isoCode();
    }

    public function __toString(): string
    {
        return sprintf('%s: %s', $this->isoCode(), $this->symbol());
    }

    public function asArray(): array
    {
        return [
            'iso_code' => $this->isoCode(),
            'symbol' => $this->symbol(),
        ];
    }

    private static function validIsoCode(string $isoCode): void
    {
        if (!(array_key_exists($isoCode, self::CURRENCIES))) {
            throw InvalidSharedException::invalidCurrencyCode(
                sprintf('Given iso code: %s, not supported or invalid', $isoCode)
            );
        }
    }
}