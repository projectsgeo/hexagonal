<?php
declare(strict_types=1);

namespace Hexagonal\Domain\Shared\ValueObject;

use Stringable;
use Symfony\Component\Uid\Uuid;

abstract class EntityUuid implements Stringable
{
    protected string $value;

    public function __construct(string $value = null)
    {
        $this->value = Uuid::fromString($value ?? (string) Uuid::v4())->__toString();
    }

    public function value(): string
    {
        return $this->value;
    }

    public function equals(EntityUuid $entityUuid): bool
    {
        return $this->value() === $entityUuid->value();
    }

    public function __toString(): string
    {
        return $this->value();
    }
}