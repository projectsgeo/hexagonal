<?php
declare(strict_types=1);

namespace Hexagonal\Domain\Shared\ValueObject;

abstract class BoolValueObject
{
    protected bool $value;

    public function __construct(bool $value)
    {
        $this->value = $value;
    }

    public static function create(bool $value): self
    {
        return new static($value);
    }

    public function value(): bool
    {
        return $this->value;
    }

    public function __toString(): string
    {
        return $this->value() ? 'True' : 'False';
    }
}