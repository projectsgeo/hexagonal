<?php
declare(strict_types=1);

namespace Hexagonal\Domain\Shared\ValueObject;

use Hexagonal\Domain\Shared\Interfaces\ArrayRepresentable;

class Address implements ArrayRepresentable
{
    private Country $country;
    private State $state;
    private City $city;
    private Location $location;
    private string $postalCode;

    public function __construct(Country $country, State $state, City $city, Location $location, string $postalCode)
    {
        $this->country = $country;
        $this->state = $state;
        $this->city = $city;
        $this->location = $location;
        $this->postalCode = $postalCode;
    }

    public function country(): Country
    {
        return $this->country;
    }

    public function state(): State
    {
        return $this->state;
    }

    public function city(): City
    {
        return $this->city;
    }

    public function location(): Location
    {
        return $this->location;
    }

    public function postalCode(): string
    {
        return $this->postalCode;
    }

    public function asArray(): array
    {
        return [
            'country'       => $this->country()->isoCode(),
            'state'         => $this->state()->__toString(),
            'location'      => $this->location()->__toString(),
            'city'          => $this->city()->__toString(),
            'postal_code'   => $this->postalCode()
        ];
    }
}