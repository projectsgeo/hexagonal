<?php
declare(strict_types=1);

namespace Hexagonal\Domain\Shared\Exception;

use Exception;

class InvalidSharedException extends Exception
{
    private string $name;

    public const INVALID_COUNTRY_ISO_CODE = 'Invalid Country Code';
    public const COUNTRY_NOT_FOUND = 'Country Not Found';
    public const INVALID_CURRENCY_CODE = 'Invalid Currency Code';
    public const MIN_MONEY_VALUE = 'Minimum Amount Value Exception';
    public const CURRENCIES_DONT_MATCH = 'Currencies do not match';
    public const ENTITY_NOT_FOUND = 'Entity Not Found';

    private function __construct(string $name, string $message = "")
    {
        parent::__construct($message);
        $this->name = $name;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function is(string $name): bool
    {
        return $this->name() === $name;
    }

    public static function invalidCountryIsoCode(string $message = ''): self
    {
        return new self(self::INVALID_COUNTRY_ISO_CODE, $message);
    }

    public static function countryNotFound(string $message = ''): self
    {
        return new self(self::COUNTRY_NOT_FOUND, $message);
    }

    public static function invalidCurrencyCode(string $message = ''): self
    {
        return new self(self::INVALID_CURRENCY_CODE, $message);
    }

    public static function minimumMoneyValueUnfulfilled(string $message = ''): self
    {
        return new self(self::MIN_MONEY_VALUE, $message);
    }

    public static function currenciesDontMatchException(string $message = ''): self
    {
        return new self(self::CURRENCIES_DONT_MATCH, $message);
    }

    public static function entityNotFound(string $message = ''): self
    {
        return new self (self::ENTITY_NOT_FOUND, 'Not found ' . $message);
    }
}