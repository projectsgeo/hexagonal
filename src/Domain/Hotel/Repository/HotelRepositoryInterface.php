<?php
declare(strict_types=1);

namespace Hexagonal\Domain\Hotel\Repository;

use Hexagonal\Domain\Hotel\Model\Hotel;
use Hexagonal\Domain\Hotel\ValueObject\HotelId;

interface HotelRepositoryInterface
{
    public function store(Hotel $hotel): void;

    public function byHotelId(HotelId $hotelId): ?Hotel;
}