<?php
declare(strict_types=1);

namespace Hexagonal\Domain\Hotel\Exception;

use Exception;
use Hexagonal\Domain\Shared\Traits\ExceptionHandler;

class InvalidHotelException extends Exception
{
    use ExceptionHandler;

    private const INVALID_DESCRIPTION_LENGTH = 'Invalid Description Length';

    public static function invalidDescriptionLength(string $message = ''): self
    {
        return new self(self::INVALID_DESCRIPTION_LENGTH, $message);
    }
}