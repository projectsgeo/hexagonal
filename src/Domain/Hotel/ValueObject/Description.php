<?php
declare(strict_types=1);

namespace Hexagonal\Domain\Hotel\ValueObject;

use Hexagonal\Domain\Hotel\Exception\InvalidHotelException;
use Hexagonal\Domain\Shared\ValueObject\StringValueObject;

class Description extends StringValueObject
{
    public const MIN_LENGTH = 5;
    public const MAX_LENGTH = 255;

    private function __construct(string $value)
    {
        parent::__construct($value);
    }

    /**
     * @throws InvalidHotelException
     */
    public static function create(string $value): self
    {
        $value = trim($value);
        self::validateLength($value);

        return new self($value);
    }

    /**
     * @throws InvalidHotelException
     */
    private static function validateLength(string $value): void
    {
        $valueLength = strlen($value);
        if ($valueLength < self::MIN_LENGTH || $valueLength > self::MAX_LENGTH) {
            throw InvalidHotelException::invalidDescriptionLength(
                sprintf('Description value is invalid. Length must be between %s and %s', self::MIN_LENGTH, self::MAX_LENGTH)
            );
        }
    }
}