<?php
declare(strict_types=1);

namespace Hexagonal\Domain\Hotel\ValueObject;

use Hexagonal\Domain\Shared\ValueObject\EntityUuid;

class HotelId extends EntityUuid
{

}