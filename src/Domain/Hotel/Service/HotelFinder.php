<?php
declare(strict_types=1);

namespace Hexagonal\Domain\Hotel\Service;

use Hexagonal\Domain\Hotel\Model\Hotel;
use Hexagonal\Domain\Hotel\Repository\HotelRepositoryInterface;
use Hexagonal\Domain\Hotel\ValueObject\HotelId;
use Hexagonal\Domain\Shared\Exception\InvalidSharedException;
use Psr\Log\LoggerInterface;

class HotelFinder implements HotelFinderInterface
{
    private HotelRepositoryInterface $hotelRepository;
    private LoggerInterface $logger;

    public function __construct(HotelRepositoryInterface $hotelRepository, LoggerInterface $logger)
    {
        $this->hotelRepository = $hotelRepository;
        $this->logger = $logger;
    }

    /**
     * @throws InvalidSharedException
     */
    public function __invoke(HotelId $hotelId): Hotel
    {
        $hotel = $this->hotelRepository->byHotelId($hotelId);
        $this->hotelExists($hotelId, $hotel);

        return $hotel;
    }

    private function hotelExists(HotelId $id, ?Hotel $hotel): void
    {
        if (null === $hotel) {
            throw InvalidSharedException::entityNotFound('Hotel with the following id: ' . $id);
        }
    }
}