<?php
declare(strict_types=1);

namespace Hexagonal\Domain\Hotel\Service;

use Hexagonal\Domain\Hotel\Model\Hotel;
use Hexagonal\Domain\Hotel\ValueObject\HotelId;

interface HotelFinderInterface
{
    public function __invoke(HotelId $hotelId): Hotel;
}