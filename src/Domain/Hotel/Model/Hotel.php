<?php
declare(strict_types=1);

namespace Hexagonal\Domain\Hotel\Model;

use Hexagonal\Domain\Hotel\ValueObject\Description;
use Hexagonal\Domain\Hotel\ValueObject\HotelId;
use Hexagonal\Domain\Shared\Interfaces\ArrayRepresentable;
use Hexagonal\Domain\Shared\ValueObject\Active;
use Hexagonal\Domain\Shared\ValueObject\Address;

class Hotel implements ArrayRepresentable
{
    private HotelId $id;
    private Address $address;
    private Description $description;
    private Active $active;

    public function __construct(HotelId $id, Address $address, Description $description, ?Active $active = null)
    {
        $this->id = $id;
        $this->address = $address;
        $this->description = $description;
        $this->active = $active ?? Active::create();
    }

    public function id(): HotelId
    {
        return $this->id;
    }

    public function address(): Address
    {
        return $this->address;
    }

    public function description(): Description
    {
        return $this->description;
    }

    public function active(): Active
    {
        return $this->active;
    }

    public function asArray(): array
    {
        return [
            'id' => $this->id()->__toString(),
            'address' => $this->address()->asArray(),
            'description' => $this->description()->__toString(),
        ];
    }


}