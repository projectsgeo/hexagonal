<?php
declare(strict_types=1);

namespace Hexagonal\Domain\Room\Repository;

use Hexagonal\Domain\Hotel\ValueObject\HotelId;
use Hexagonal\Domain\Room\Model\Room;

interface RoomRepositoryInterface
{
    public function save(Room $room): void;

    public function byHotelId(HotelId $hotelId): ?array;
}