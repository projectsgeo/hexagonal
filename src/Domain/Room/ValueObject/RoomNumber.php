<?php
declare(strict_types=1);

namespace Hexagonal\Domain\Room\ValueObject;

use Hexagonal\Domain\Shared\ValueObject\IntValueObject;

class RoomNumber extends IntValueObject
{

}