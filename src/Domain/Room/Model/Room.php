<?php
declare(strict_types=1);

namespace Hexagonal\Domain\Room\Model;

use Hexagonal\Domain\Hotel\Model\Hotel;
use Hexagonal\Domain\Room\ValueObject\RoomNumber;
use Hexagonal\Domain\Room\ValueObject\Floor;
use Hexagonal\Domain\Room\ValueObject\RoomId;
use Hexagonal\Domain\Shared\Interfaces\ArrayRepresentable;
use Hexagonal\Domain\Shared\ValueObject\Active;
use Hexagonal\Domain\Shared\ValueObject\Money;

class Room implements ArrayRepresentable
{
    private RoomId $id;
    private Hotel $hotel;
    private Floor $floor;
    private RoomNumber $number;
    private Money $price;
    private Active $active;

    public function __construct(RoomId $id, Hotel $hotel, Floor $floor, RoomNumber $number, Money $price, Active $active = null)
    {
        $this->id = $id;
        $this->hotel = $hotel;
        $this->floor = $floor;
        $this->number = $number;
        $this->price = $price;
        $this->active = $active ?? Active::create();
    }

    public function id(): RoomId
    {
        return $this->id;
    }

    public function hotel(): Hotel
    {
        return $this->hotel;
    }

    public function floor(): Floor
    {
        return $this->floor;
    }

    public function number(): RoomNumber
    {
        return $this->number;
    }

    public function price(): Money
    {
        return $this->price;
    }

    public function active(): Active
    {
        return $this->active;
    }

    public function asArray(): array
    {
        return [
            'id' => $this->id(),
            'hotel' => $this->hotel()->asArray(),
            'floor' => $this->floor(),
            'number' => $this->number(),
            'price' => $this->price()->render(),
        ];
    }
}