<?php
declare(strict_types=1);

namespace Hexagonal\UserInterface\Shared\Exception;

use Exception;
use Hexagonal\Domain\Shared\Traits\ExceptionHandler;

class HexagonalRequestException extends Exception
{
    use ExceptionHandler;

    public const INVALID_JSON_PAYLOAD_EXCEPTION = 'Invalid Json Payload Exception';
    public const INVALID_PAYLOAD_EXCEPTION = 'Invalid Data in Payload Exception';
    public const INVALID_HTTP_PARAMETER_EXCEPTION = 'Invalid Data in Parameter Exception';

    private function __construct(string $name, string $message = '')
    {
        parent::__construct($message);
        $this->name = $name;
    }

    public static function InvalidJsonPayload(string $message =""): self
    {
        return new self(self::INVALID_JSON_PAYLOAD_EXCEPTION, 'Error decoding json: ' . $message);
    }

    public static function InvalidPayload(string $message = ""): self
    {
        return new self(self::INVALID_PAYLOAD_EXCEPTION, $message);
    }

    public static function InvalidParameter(string $message = ""): self
    {
        return new self(self::INVALID_HTTP_PARAMETER_EXCEPTION, $message);
    }
}