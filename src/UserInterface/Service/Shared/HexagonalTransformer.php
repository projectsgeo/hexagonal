<?php
declare(strict_types=1);

namespace Hexagonal\UserInterface\Service\Shared;

use Hexagonal\Application\Shared\Contract\HexagonalRequestInterface;
use Hexagonal\UserInterface\Shared\Exception\HexagonalRequestException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class HexagonalTransformer
{
    protected ValidatorInterface $validator;
    protected LoggerInterface $logger;

    public function __construct(ValidatorInterface $validator, LoggerInterface $logger)
    {
        $this->validator = $validator;
        $this->logger = $logger;
    }

    abstract protected function buildRequest(array $payload): HexagonalRequestInterface;

    /**
     * @throws HexagonalRequestException
     */
    public function fromJsonHttpRequest(Request $httpRequest): HexagonalRequestInterface
    {
        $arrayRequest = $this->jsonDecode($httpRequest);
        return $this->fromArrayRequest($arrayRequest);
    }

    public function fromArrayRequest(array $arrayRequest): HexagonalRequestInterface
    {
        try {
            $paymentRequest = $this->buildRequest($arrayRequest);
            $this->validate($paymentRequest);
            return $paymentRequest;
        } catch (HexagonalRequestException $exception) {
            $this->logger->error($exception->getMessage(), [$exception->getTrace(),]);
            throw $exception;
        }
    }

    private function validate(HexagonalRequestInterface $request): void
    {
        $errors = $this->validator->validate($request);
        if (count($errors) > 0) {
            $message = '';
            /** @var ConstraintViolationInterface $error */
            foreach ($errors as $error) {
                $message .= $error->getPropertyPath() . ': ' . $error->getMessage();
            }
            throw HexagonalRequestException::InvalidPayload($message);
        }
    }

    private function jsonDecode(Request $httpRequest): array
    {
        $httpRequestPayload = json_decode((string)$httpRequest->getContent(), true);
        if (JSON_ERROR_NONE !== json_last_error()) {
            $this->logger->error(json_last_error_msg());
            throw HexagonalRequestException::InvalidJsonPayload(json_last_error_msg());
        }

        return $httpRequestPayload;
    }
}