<?php
declare(strict_types=1);

namespace Hexagonal\UserInterface\Service\Hotel;

use Exception;
use Hexagonal\Application\Hotel\Dto\CreateHotelRequest;
use Hexagonal\Application\Shared\Contract\HexagonalRequestInterface;
use Hexagonal\Domain\Hotel\Exception\InvalidHotelException;
use Hexagonal\Domain\Hotel\ValueObject\Description;
use Hexagonal\Domain\Shared\Exception\InvalidSharedException;
use Hexagonal\Domain\Shared\ValueObject\Address;
use Hexagonal\Domain\Shared\ValueObject\City;
use Hexagonal\Domain\Shared\ValueObject\Country;
use Hexagonal\Domain\Shared\ValueObject\Location;
use Hexagonal\Domain\Shared\ValueObject\State;
use Hexagonal\UserInterface\Service\Shared\HexagonalTransformer;
use Hexagonal\UserInterface\Shared\Exception\HexagonalRequestException;
use TypeError;

class CreateHotelTransformer extends HexagonalTransformer
{
    /**
     * @throws HexagonalRequestException
     */
    protected function buildRequest(array $payload): HexagonalRequestInterface
    {
        try {
            return new CreateHotelRequest(
                new Address(
                    Country::fromIsoCode($payload['country']),
                    new State($payload['state']),
                    new City($payload['city']),
                    new Location($payload['location']),
                    $payload['postal_code']
                ),
                Description::create($payload['description'])
            );
        } catch (InvalidSharedException | InvalidHotelException $exception) {
            throw HexagonalRequestException::InvalidPayload($exception->getMessage());
        } catch (Exception | TypeError $exception) {
            throw HexagonalRequestException::InvalidParameter($exception->getMessage());
        }
    }
}