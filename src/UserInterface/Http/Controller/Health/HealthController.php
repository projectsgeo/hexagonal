<?php
declare(strict_types=1);

namespace Hexagonal\UserInterface\Http\Controller\Health;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

final class HealthController extends AbstractController
{
    private const LAST_COMMIT = "git log -1 --pretty=format:'%h - %s (%ci)' --abbrev-commit";

    public function __invoke(): JsonResponse
    {
        return new JsonResponse(
            [
                'status' => 'ready',
                'service_name' => $this->getParameter('service_name'),
                'version' => shell_exec(self::LAST_COMMIT) ?? $this->getParameter('service_version'),
                'docker_node' => gethostname()
            ],
            JsonResponse::HTTP_OK
        );
    }
}
