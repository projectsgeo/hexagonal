<?php
declare(strict_types=1);

namespace Hexagonal\UserInterface\Http\Controller\Hotel;

use Hexagonal\Application\Hotel\CreateHotelHandler;
use Hexagonal\Application\Shared\Dto\HexagonalResponse;
use Hexagonal\UserInterface\Service\Hotel\CreateHotelTransformer;
use Hexagonal\UserInterface\Shared\Exception\HexagonalRequestException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CreateHotelController extends AbstractController
{
    private CreateHotelTransformer $hotelTransformer;
    private CreateHotelHandler $createHotelHandler;

    public function __construct(CreateHotelTransformer $hotelTransformer, CreateHotelHandler $createHotelHandler)
    {
        $this->hotelTransformer = $hotelTransformer;
        $this->createHotelHandler = $createHotelHandler;
    }

    public function __invoke(Request $request)
    {
        try {
            $createHotelRequest = $this->hotelTransformer->fromJsonHttpRequest($request);
            $response = ($this->createHotelHandler)($createHotelRequest);

            return new JsonResponse(
                $response->asArray(),
                $response->isStatusOk() ? JsonResponse::HTTP_CREATED : JsonResponse::HTTP_UNPROCESSABLE_ENTITY
            );
        } catch (HexagonalRequestException $exception) {
            return new JsonResponse(
                HexagonalResponse::fromNull($exception->getMessage())->asArray(),
                JsonResponse::HTTP_BAD_REQUEST
            );
        }
    }
}