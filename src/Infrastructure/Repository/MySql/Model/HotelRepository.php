<?php
declare(strict_types=1);

namespace Hexagonal\Infrastructure\Repository\MySql\Model;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use Hexagonal\Domain\Hotel\Model\Hotel;
use Hexagonal\Domain\Hotel\Repository\HotelRepositoryInterface;
use Hexagonal\Domain\Hotel\ValueObject\HotelId;

class HotelRepository extends ServiceEntityRepository implements HotelRepositoryInterface
{
    public function __construct(ManagerRegistry $registry, string $entityClass = Hotel::class)
    {
        parent::__construct($registry, $entityClass);
    }

    public function store(Hotel $hotel): void
    {
        $this->_em->persist($hotel);
        $this->_em->flush();
    }

    /**
     * @throws NonUniqueResultException
     */
    public function byHotelId(HotelId $hotelId): ?Hotel
    {
        return $this->createQueryBuilder('hotel')
            ->where('hotel.id = :hotelId')
            ->andWhere('hotel.active.value = true')
            ->setParameter('hotelId', $hotelId)
            ->getQuery()
            ->getOneOrNullResult();
    }
}