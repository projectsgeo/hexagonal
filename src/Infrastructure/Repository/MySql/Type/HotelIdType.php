<?php
declare(strict_types=1);

namespace Hexagonal\Infrastructure\Repository\MySql\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;
use Hexagonal\Domain\Hotel\ValueObject\HotelId;

class HotelIdType extends StringType
{
    private const NAME = 'hotel_id';

    public function getName()
    {
        return self::NAME;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return new HotelId($value);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value->__toString();
    }
}