<?php

declare(strict_types=1);

namespace Hexagonal\Infrastructure\Repository\MySql\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201020190503 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE hotel (id VARCHAR(36) NOT NULL, description VARCHAR(255) NOT NULL, postal_code VARCHAR(255) NOT NULL, country_iso_code VARCHAR(2) NOT NULL, country_name VARCHAR(255) NOT NULL, state VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, location VARCHAR(255) NOT NULL, active TINYINT(1) DEFAULT \'1\' NOT NULL, INDEX country_idx (country_name, country_iso_code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE hotel');
    }
}
