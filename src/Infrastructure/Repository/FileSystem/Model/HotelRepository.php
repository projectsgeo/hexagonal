<?php
declare(strict_types=1);

namespace Hexagonal\Infrastructure\Repository\FileSystem\Model;

use Hexagonal\Domain\Hotel\Model\Hotel;
use Hexagonal\Domain\Hotel\Repository\HotelRepositoryInterface;
use Hexagonal\Domain\Hotel\ValueObject\HotelId;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

class HotelRepository extends FilesystemAdapter implements HotelRepositoryInterface
{
    private const HOTEL_ITEM_KEY = 'hotel_';

    public function __construct(string $namespaceCache, int $defaultLifetimeCache, string $directoryCache)
    {
        parent::__construct($namespaceCache, $defaultLifetimeCache, $directoryCache);
    }

    public function store(Hotel $hotel): void
    {
        $item = $this->getItem(self::HOTEL_ITEM_KEY . $hotel->id()->__toString());
        $item->set($hotel);
        $this->save($item);
    }

    public function byHotelId(HotelId $hotelId): ?Hotel
    {
        $item = $this->getItem(self::HOTEL_ITEM_KEY . $hotelId->value());
        return $item->isHit() ? $item->get() : null;
    }
}