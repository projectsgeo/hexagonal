<?php
declare(strict_types=1);

namespace Hexagonal\Application\Shared\Dto;

class HexagonalResponse
{
    public const STATUS_KO = 'error';
    public const STATUS_OK = 'ready';
    public const NULL_MESSAGE = 'Something went wrong.';
    public const NOT_FOUND_MESSAGE = 'Not Found';

    private string $message;
    private string $status;

    protected function __construct(string $status, string $message)
    {
        $this->status = $status;
        $this->message = $message;
    }

    public static function fromNull(string $message = '')
    {
        return new static(self::STATUS_KO, self::NULL_MESSAGE . ' ' . $message);
    }

    public static function fromNotFound(string $message)
    {
        return new static(self::STATUS_KO, self::NOT_FOUND_MESSAGE . ' ' . $message);
    }

    public function message(): string
    {
        return $this->message;
    }

    public function status(): string
    {
        return $this->status;
    }

    public function isStatusOk(): bool
    {
        return $this->status === self::STATUS_OK;
    }

    public function asArray(): array
    {
        return [
            'status' => $this->status(),
            'message' => $this->message(),
        ];
    }
}