<?php
declare(strict_types=1);

namespace Hexagonal\Application\Hotel\Dto;

use Hexagonal\Application\Shared\Dto\HexagonalResponse;
use Hexagonal\Domain\Hotel\Model\Hotel;

class HotelResponse extends HexagonalResponse
{
    private ?Hotel $hotel;

    protected function __construct(string $status, string $message, Hotel $hotel = null)
    {
        parent::__construct($status, $message);
        $this->hotel = $hotel;
    }

    public static function fromHotel(Hotel $hotel, string $message = ''): self
    {
        return new self(self::STATUS_OK, $message, $hotel);
    }

    public function hotel(): ?Hotel
    {
        return $this->hotel;
    }

    public function asArray(): array
    {
        return array_merge(
            parent::asArray(),
            [
                'hotel' => ($this->hotel() instanceof Hotel ? $this->hotel()->asArray() : null)
            ]
        );
    }
}