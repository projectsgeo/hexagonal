<?php
declare(strict_types=1);

namespace Hexagonal\Application\Hotel\Dto;

use Hexagonal\Application\Shared\Contract\HexagonalRequestInterface;
use Hexagonal\Domain\Shared\ValueObject\Address;
use Hexagonal\Domain\Hotel\ValueObject\Description;

class CreateHotelRequest implements HexagonalRequestInterface
{
    private Address $address;
    private Description $description;

    public function __construct(Address $address, Description $description)
    {
        $this->address = $address;
        $this->description = $description;
    }

    public function address(): Address
    {
        return $this->address;
    }

    public function description(): Description
    {
        return $this->description;
    }
}