<?php
declare(strict_types=1);

namespace Hexagonal\Application\Hotel;

use Exception;
use Hexagonal\Application\Hotel\Dto\CreateHotelRequest;
use Hexagonal\Application\Hotel\Dto\HotelResponse;
use Hexagonal\Application\Shared\Contract\HexagonalRequestInterface;
use Hexagonal\Domain\Hotel\Model\Hotel;
use Hexagonal\Domain\Hotel\Repository\HotelRepositoryInterface;
use Hexagonal\Domain\Hotel\ValueObject\HotelId;
use Psr\Log\LoggerInterface;

class CreateHotelHandler
{
    private HotelRepositoryInterface $hotelRepository;
    private LoggerInterface $logger;

    public function __construct(HotelRepositoryInterface $hotelRepository, LoggerInterface $logger)
    {
        $this->hotelRepository = $hotelRepository;
        $this->logger = $logger;
    }

    public function __invoke(HexagonalRequestInterface $createHotelDto): HotelResponse
    {
        try {
            $hotel = new Hotel(
                new HotelId(),
                $createHotelDto->address(),
                $createHotelDto->description(),
            );

            $this->hotelRepository->store($hotel);

            $message = 'Hotel created successfully with the following id: ' . $hotel->id();
            $this->logger->info($message);

            return HotelResponse::fromHotel($hotel, $message);
        } catch (Exception $exception) {
            $this->logger->error($exception->getMessage(), $exception->getTrace());
            return HotelResponse::fromNull($exception->getMessage());
        }
    }
}