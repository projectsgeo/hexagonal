<?php
declare(strict_types=1);

namespace Hexagonal\Application\Room\Dto;

use Hexagonal\Domain\Hotel\ValueObject\HotelId;
use Hexagonal\Domain\Room\ValueObject\RoomNumber;
use Hexagonal\Domain\Room\ValueObject\Floor;
use Hexagonal\Domain\Shared\ValueObject\Money;

class CreateRoomRequest
{
    private HotelId $hotelId;
    private Floor $floor;
    private RoomNumber $number;
    private Money $price;

    public function __construct(HotelId $hotelId, Floor $floor, RoomNumber $number, Money $price)
    {
        $this->hotelId = $hotelId;
        $this->floor = $floor;
        $this->number = $number;
        $this->price = $price;
    }

    public function hotelId(): HotelId
    {
        return $this->hotelId;
    }

    public function floor(): Floor
    {
        return $this->floor;
    }

    public function roomNumber(): RoomNumber
    {
        return $this->number;
    }

    public function price(): Money
    {
        return $this->price;
    }
}