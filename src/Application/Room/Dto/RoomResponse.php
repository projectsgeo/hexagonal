<?php
declare(strict_types=1);

namespace Hexagonal\Application\Room\Dto;

use Hexagonal\Application\Shared\Dto\HexagonalResponse;
use Hexagonal\Domain\Room\Model\Room;

class RoomResponse extends HexagonalResponse
{
    private ?Room $room;

    protected function __construct(string $status, string $message, Room $room = null)
    {
        parent::__construct($status, $message);
        $this->room = $room;
    }

    public static function fromRoom(Room $room, string $message = ''): self
    {
        return new self(self::STATUS_OK, $message, $room);
    }

    public function room(): ?Room
    {
        return $this->room;
    }

    public function asArray(): array
    {
        return array_merge(
            parent::asArray(),
            [
                'room' => ($this->room() instanceof Room ? $this->room()->asArray() : null)
            ]
        );
    }
}