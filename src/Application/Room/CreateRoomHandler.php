<?php
declare(strict_types=1);

namespace Hexagonal\Application\Room;

use Hexagonal\Application\Room\Dto\CreateRoomRequest;
use Hexagonal\Application\Room\Dto\RoomResponse;
use Hexagonal\Domain\Hotel\Service\HotelFinder;
use Hexagonal\Domain\Room\Model\Room;
use Hexagonal\Domain\Room\Repository\RoomRepositoryInterface;
use Hexagonal\Domain\Room\ValueObject\RoomId;
use Hexagonal\Domain\Shared\Exception\InvalidSharedException;
use Psr\Log\LoggerInterface;
use Exception;

class CreateRoomHandler
{
    private RoomRepositoryInterface $roomRepository;
    private HotelFinder $hotelFinder;
    private LoggerInterface $logger;

    public function __construct(RoomRepositoryInterface $roomRepository, HotelFinder $hotelFinder, LoggerInterface $logger)
    {
        $this->roomRepository = $roomRepository;
        $this->hotelFinder = $hotelFinder;
        $this->logger = $logger;
    }

    public function __invoke(CreateRoomRequest $createRoomRequest): RoomResponse
    {
        try {
            $hotel = $this->hotelFinder->__invoke($createRoomRequest->hotelId());
            //$hotel = ($this->hotelFinder)($createRoomRequest->hotelId());
            $room = new Room(
                new RoomId(),
                $hotel,
                $createRoomRequest->floor(),
                $createRoomRequest->roomNumber(),
                $createRoomRequest->price(),
            );

            $this->roomRepository->save($room);

            $message = sprintf('Room created to hotel %s successfully with following id: %s', $room->hotel()->id(), $room->id());
            $this->logger->info($message);

            return RoomResponse::fromRoom($room, $message);
        } catch (InvalidSharedException | Exception $exception) {
            $this->logger->error($exception->getMessage(), $exception->getTrace());
            return RoomResponse::fromNull($exception->getMessage());
        }
    }
}